import os
import telebot
import crypto_price as CP
from telebot import types
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())
bot = telebot.TeleBot(os.getenv("TOKEN"))


@bot.message_handler(commands=["start"])
def start(msg):
    bot.send_message(
        msg.chat.id, f"Hello, {msg.from_user.first_name}! Nice to meet you! I'm Crypto Lady-Cat Bot!")


@bot.message_handler(commands=["website"])
def website(msg):
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Go to Binance!",
                                          url="https://www.binance.com/en"))
    bot.send_message(msg.chat.id, "View available crypto offers.", reply_markup=markup)


@bot.message_handler(commands=["price"])
def price(msg):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    eth_button = types.KeyboardButton("ETH")
    btc_button = types.KeyboardButton("BTC")
    markup.add(eth_button, btc_button)
    bot.send_message(msg.chat.id, "Select the cryptocurrency whose price you want to receive.", reply_markup=markup)


@bot.message_handler(content_types=["text"])
def get_user_text(msg):
    markup = types.ReplyKeyboardRemove(selective=False)
    if msg.text.lower() == "eth":
        url = "https://www.binance.com/en/price/ethereum"
        result = CP.get_price(url)
        bot.send_message(msg.chat.id, text=result, reply_markup=markup)
    elif msg.text.lower() == "btc":
        url = "https://www.binance.com/en/price/bitcoin"
        result = CP.get_price(url)
        bot.send_message(msg.chat.id, text=result, reply_markup=markup)
    else:
        bot.send_sticker(
            msg.chat.id, sticker=os.getenv("STICKER-CONTENT-TYPE"), reply_markup=markup)


bot.polling(non_stop=True)
