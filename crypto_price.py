from bs4 import BeautifulSoup as BS
import requests


def get_price(url):
    data = requests.get(url)
    soup = BS(data.text, 'html.parser')
    result = soup.find("div", class_="css-12ujz79").text
    return result


